package tests;

import hometask8.Family;
import hometask8.Fish;
import hometask8.Human;
import hometask8.Pet;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    private Human module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    @Test
    public void  humanToString(){

        module = new Human("human","surname",1995);
        String actual = module.toString();
        String expected = "Human{name = human surname = surname year = 1995 iq = 0 schedule = null}";
        assertEquals(expected,actual);
    }
    @Test

    public void testHumanGreetPet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mira = new Fish("Mira",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mira));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.greetPet(Mira);
        assertEquals(output.toString().replaceAll("\n",""),"Привет Mira","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testHumanDescribePet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mura = new Fish("Mira",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mura));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.describePet(Mura);
        assertEquals(output.toString().replaceAll("\n",""),"У меня есть FISH, ему 5 лет, он очень хитрый","Successfully brings text");

        System.setOut(old);
    }
}