package tests;

import hometask8.Dog;
import hometask8.Family;
import hometask8.Human;
import hometask8.Pet;
import org.junit.jupiter.api.*;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FamilyTest {
    private Family module;

    @BeforeEach
    public void setUp() {
        Human mother = new Human("mother","motherSurname",1978);
        Human father = new Human("father","fatherSurname",1976);
        Pet pet = new Dog("pet",3,(byte) 20);
        HashMap <DayOfWeek,String> schedule = new HashMap< >();
        mother.setSchedule(schedule);
        father.setSchedule(schedule);
        module = new Family(mother,father);
        Human child = new Human("child","childSurname",1998);
        child.setSchedule(schedule);
        module.addChild(child);
        Set pets = new HashSet<>(Set.of(pet));
        module.setPet(pets);

    }

    @Test
    public void testFamilyToString(){

        String actual = module.toString();
        String expected = "Human{name = child surname = childSurname year = 1998 iq = 0 schedule = {}}"+
                "Human{name = father surname = fatherSurname year = 1976 iq = 0 schedule = {}}"+
                "Human{name = mother surname = motherSurname year = 1978 iq = 0 schedule = {}}"+
                "[DOG{nickname=pet, age=3, trickLevel=20 , habits=null}]";
        assertEquals(expected,actual);

    }
    @Test
    public void testCountFamily() {

        int actual = module.countFamily((ArrayList<Human>) module.getChildren());
        int expected = 3;
        assertEquals(expected,actual);

    }
    @Test
    public void testAddChild(){
        Human child2 = new Human("child2","surname",1999);
        module.addChild(child2);
        int actual = module.getChildren().size();
        int expected = 2;
        assertEquals(expected,actual);
    }
    @Test
    public void testDeleteChildWithWrongIndex(){

        int length = module.getChildren().size();
        int index = length + 1;
        module.deleteChild(index);
        int actual = module.getChildren().size();
        int expected = length;
        assertEquals(expected, actual);
    }
    @Test
    public void testDeleteChildIndex(){

        int length = module.getChildren().size();
        boolean bool = module.deleteChild(0);
        int actual = module.getChildren().size();
        int expected = length - 1;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = true;
        assertEquals(expected2,actual2);
    }
    @Test
    public void testDeleteChildHuman(){
        Human child3 = new Human("sam","smith",32);
        module.addChild(child3);
        int length = module.getChildren().size();
        boolean bool = module.deleteChild(child3);
        int actual = module.getChildren().size();
        int expected = length -1 ;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = true;
        assertEquals(expected2,actual2);
    }
    @Test
    public void testDeleteChildHumanWrongObject(){
        Human child3 = new Human();
        int length = module.getChildren().size();
        boolean bool = module.deleteChild(child3);
        int actual = module.getChildren().size();
        int expected = length ;
        assertEquals(expected, actual);
        boolean actual2 = bool;
        boolean expected2 = false;
        assertEquals(expected2,actual2);

    }


}


