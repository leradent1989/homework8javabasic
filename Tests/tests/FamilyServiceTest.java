package tests;

import hometask8.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    private FamilyService module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    List<Family> families = new ArrayList<>(List.of(
            new Family(new Human("gerge1", "grgr1", 1973), new Human("gdgd1", "dgdgd1", 1978)),
            new Family(new Human("gerge2", "grgr2", 1973), new Human("gdgd2", "dgdgd2", 1978)),
            new Family(new Human("gerge3", "grgr3", 1973), new Human("gdgd3", "dgdgd3", 1978)),
            new Family(new Human("gerge4", "grgr4", 1973), new Human("gdgd4", "dgdgd4", 1978)),
            new Family(new Human("gerge5", "grgr5", 1973), new Human("gdgd5", "dgdgd5", 1978)),
            new Family(new Human("gerge6", "grgr6", 1973), new Human("gdgd6", "dgdgd6", 1978))
    ));

    @BeforeEach
    public void setUp() {

        CollectionFamilyDao familyDao = new CollectionFamilyDao(families);
        module = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(module);


    }

    @Test
    void getAllFamilies() {
        List<Family> actual = module.getAllFamilies();
        List<Family> expected = families;
        assertEquals(expected, actual);
    }

    @Test
    void displayAllFamilies() {
        PrintStream old = System.out;

        System.setOut(new PrintStream(output));
        module.displayAllFamilies();
        assertEquals(output.toString().replaceAll("\n", ""), "Human{name = gdgd1 surname = dgdgd1 year = 1978 iq = 0 schedule = null}Human{name = gerge1 surname = grgr1 year = 1973 iq = 0 schedule = null}" +
                "Human{name = gdgd2 surname = dgdgd2 year = 1978 iq = 0 schedule = null}" +
                "Human{name = gerge2 surname = grgr2 year = 1973 iq = 0 schedule = null}" +
                "Human{name = gdgd3 surname = dgdgd3 year = 1978 iq = 0 schedule = null}" +
                "Human{name = gerge3 surname = grgr3 year = 1973 iq = 0 schedule = null}" +
                "Human{name = gdgd4 surname = dgdgd4 year = 1978 iq = 0 schedule = null}" +
                "Human{name = gerge4 surname = grgr4 year = 1973 iq = 0 schedule = null}" +
                "Human{name = gdgd5 surname = dgdgd5 year = 1978 iq = 0 schedule = null}" +
                "Human{name = gerge5 surname = grgr5 year = 1973 iq = 0 schedule = null}" +
                "Human{name = gdgd6 surname = dgdgd6 year = 1978 iq = 0 schedule = null}" +
                "Human{name = gerge6 surname = grgr6 year = 1973 iq = 0 schedule = null}", "Successfully brings text");

        System.setOut(old);
    }


    @Test
    void getFamilyById() {
        Family actual = module.getFamilyById(1);
        Family expected = families.get(1);
        assertEquals(expected, actual);
    }

    @Test
    void count() {
        int actual = module.count();
        int expected = families.size();
        assertEquals(expected, actual);
    }

    @Test
    void createNewFamily() {
        int size = families.size();
        module.createNewFamily(new Human("mother", "dgrg7", 23), new Human("father", "grrr7", 30));
        int actual = families.size();
        int expected = size + 1;
        assertEquals(expected, actual);
    }

    @Test
    void deleteFamilyByIndex() {
        Family actual = families.get(2);
        module.deleteFamilyByIndex(1);
        Family expected = families.get(1);
        assertEquals(expected, actual);
    }

    @Test
    void getPets() {
        Set<Pet> actual = module.getPets(2);
        Set<Pet> expected = families.get(2).getPet();
        assertEquals(expected, actual);
    }

    @Test
    void addPet() {
        Pet pet = new Dog("snoopy", 3, (byte) 65);
        module.addPet(2, pet);
        Set<Pet> actual = new HashSet<Pet>(Set.of(pet));
        Set<Pet> expected = families.get(2).getPet();
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesBiggerThen() {
        List<Family> actual = module.getFamiliesBiggerThen(1);
        List<Family> expected = new ArrayList<>();
        List<Family> newFamilies = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) > 1) {
                newFamilies.add(family);
            }
        });
        expected = newFamilies;
        assertEquals(expected, actual);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int actual = module.countFamiliesWithMemberNumber(2);
        int expected = 6;
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesLessThen() {
        List<Family> actual = module.getFamiliesLessThen(3);
        List<Family> expected = new ArrayList<>();
        List<Family> newFamilies2 = new ArrayList<>();
        families.forEach(family -> {
            if (family.countFamily(family.getChildren()) < 3) {
                newFamilies2.add(family);
            }
        });
        expected = newFamilies2;
        assertEquals(expected, actual);
    }

    @Test
    void adoptChild() {
        Human actual = module.adoptChild(families.get(1), new Human("child", "surname", 3))
                .getChildren().get(0);
        Human expected = families.get(1).getChildren().get(0);
        assertEquals(expected, actual);
    }
   @Test
   void bornChild(){
        module.bornChild(families.get(1),"Jane","Sam");
        String name = families.get(1).getChildren().get(0).getName();
        boolean actual;
        if(name.equals("Jane") || name.equals("Sam")){
            actual = true;
        } else actual = false;
        boolean expected = true;
        assertEquals(expected,actual);
   }
    @Test
    void deleteAllChildrenOlderThen() {
        for (int i = 0; i < families.size(); i++) {
            families.get(i).getChildren().add(new Human("fggd", "dgddg", 2018));
            families.get(i).getChildren().add(new Human("fggd", "dgddg",2010 ));
        }
        boolean actual;
        int age = 10;
        module.deleteAllChildrenOlderThen(age);
        List<Human> olderChildren = new ArrayList<>();

        Calendar calendar = new GregorianCalendar();
        int yearNow = calendar.get(Calendar.YEAR);

        for (int i = 0; i < families.size(); i++) {

            for (int k = 0; k < families.get(i).getChildren().size(); k++) {

                if ((yearNow - families.get(i).getChildren().get(k).getYear()) > age) {
                    olderChildren.add(families.get(i).getChildren().get(k));
                } else continue;
            }
        }

        if(olderChildren.size()>0){
            actual = false;
        }else actual =true;
        boolean expected = true;
          assertEquals(expected,actual);
    }


}







