package hometask8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        List<Family> families = new ArrayList<>(List.of(
                new Family(new Human("gerge1","grgr1",1973),new Human("gdgd1","dgdgd1",1978)),
                new Family(new Human("gerge2","grgr2",1973),new Human("gdgd2","dgdgd2",1978)),
                new Family(new Human("gerge3","grgr3",1973),new Human("gdgd3","dgdgd3",1978)),
                new Family(new Human("gerge4","grgr4",1973),new Human("gdgd4","dgdgd4",1978)),
                new Family(new Human("gerge5","grgr5",1973),new Human("gdgd5","dgdgd5",1978)),
                new Family(new Human("gerge6","grgr6",1973),new Human("gdgd6","dgdgd6",1978))
        ));
        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);


        // 1 getAllFamilies
        System.out.println("getAllFamilies: ");

     familyController.getAllFamilies().forEach(family -> family.toString());

      System.out.println(" 2 ----------------------------------------------------");
     // 2 displayAllFamilies
        System.out.println("displayAllFamilies: ");

        familyController.displayAllFamilies();

       //3  Count
        System.out.println("count all families: ");

        System.out.println(familyController.count());

       //4 getFamilyById
        System.out.println("getFamilyById: ");

       familyController.getFamilyById(2).toString();

       System.out.println("------------------------------------------------");
        //5 adoptChild
        System.out.println("adoptChild: ");

        familyController.adoptChild( families.get(2),new Human("michael","smith",2010));

        familyController.getFamilyById(2).toString();

        System.out.println("---------------------------------------------");

        //6 bornChild
        System.out.println("bornChild: ");

        System.out.println("Family before applying born child: ");
        families.get(0).toString();
        familyController.bornChild(families.get(0),"Peter","Mary");
        System.out.println("Family after applying born child");
        families.get(0).toString();
        System.out.println("---------------------------------------------------------");
        // 7 getFamiliesLessThen
        System.out.println("getFamiliesLessThen: ");

      familyController.getFamiliesLessThen(3).forEach(family -> family.toString());

      System.out.println("------------------------------------------------");
      // 8 getFamiliesBiggerThen
        System.out.println("getFamiliesBiggerThen: ");

        familyController.getFamiliesBiggerThen(2).forEach(family -> family.toString());

        System.out.println("9 ----------------------------------------------------");

        // 9 countFamilyWithMemberNumber
        System.out.println("countFamiliesWithMemberNumber: ");

        System.out.println("Number of families:" + families.size());
        System.out.println("------------------------------");
        System.out.println("Number of families with 3 members: " + familyController.countFamiliesWithMemberNumber(3));
     // 10 CreateNewFamily
        System.out.println("createNewFamily: ");

        System.out.println("Last family in list before applying method :");
       families.get(families.size() -1).toString();
       System.out.println("Last family in list after applying method :");
        familyController.createNewFamily(new Human("vegg7","drr7",1995),new Human("gerg7","dsgt7",1990));

    families.get(families.size() -1).toString();
        // 11 Add pet

        System.out.println("addPet: ");

        familyController.addPet(0,new DomesticCat("Mura",5,(byte) 56));
        familyController.addPet(0,new DomesticCat("Pusha",7,(byte) 36));

        familyController.getFamilyById(0).toString();
    // 12 getPets
        System.out.println("getPets: ");

    System.out.println("Pets of the first family in list:");
    Arrays.toString( familyController.getPets(0).toArray());

    // 13 Delete family by index :
        System.out.println("deleteFamilyByIndex: ");

        System.out.println(" Family in list with index 2  before applying method: ");
        families.get(2).toString();
        familyController.deleteFamilyByIndex(2);
        System.out.println(" Family in list with index 2  after applying method: ");
        families.get(2).toString();

     //14 DeleteAllChildrenOlderThen
     System.out.println("DeleteAllChildrenOlderThen : first apply method  - next try to find children with age more then this age in families and if present add them to new list: ");

     for (int i = 0; i < families.size(); i++) {
        families.get(i).getChildren().add(new Human("child1", "child1", 2018));
        families.get(i).getChildren().add(new Human("child2", "child2",2010 ));
    }

    int age = 10;

        familyController.deleteAllChildrenOlderThen(age);

    List<Human> olderChildren = new ArrayList<>();
    int yearNow = LocalDate.now().getYear();

        for (int i = 0; i < families.size(); i++) {

        for (int k = 0; k < families.get(i).getChildren().size(); k++) {

            if ((yearNow - families.get(i).getChildren().get(k).getYear()) > age) {
                olderChildren.add(families.get(i).getChildren().get(k));
            } else continue;
        }
    }
        System.out.println("Children older then 10 years in list families after applying method ");
       System.out.println(olderChildren +" - there is no children more then 10 years,method works right");
       System.out.println("Families after applying method(there is no children older then 10 years:");
       families.forEach(family -> {
           System.out.println("----------------------------------------");
           family.toString();
       });
}

}
