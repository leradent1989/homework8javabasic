package hometask8;

import java.time.DayOfWeek;
import java.util.HashMap;
import java.util.Map;


 public class Human {

    private    String name;
    private    String surname;
    private    int year;
    private    byte iq;
    private    Family family;
    private   Map <DayOfWeek,String> schedule;

    public  Human(){

    }

    public  Human( String name, String surname, int year){
        this.name =name;
        this.surname = surname;
        this.year = year;
    }

    public  Human(String name,String surname,int year,Family family){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }

    public  Human(String name,String surname,int year,Family family,HashMap <DayOfWeek,String> shedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
        this.schedule = shedule;
    }
    public   String  getName(){
        return  name;

    }

    public void setName(String humanName){

        name = humanName;
    }

    public  String  getSurname(){
        return  surname;

    }
    public void setSurname(String humanSurname){

        surname = humanSurname;
    }

    public  int  getYear(){

        return  year;
    }

    public void setYear(int humanYear){

        year = humanYear;
    }


    public Family getFamily(){
        return family;
    }

    public  void setFamily(Family humanFamily){
        family = humanFamily;

    }
    public HashMap<DayOfWeek, String> getSchedule(){

        return (HashMap<DayOfWeek, String>) schedule;
    }
    public  void setSchedule (HashMap<DayOfWeek,String> humanSchedule){
        schedule = humanSchedule;
    }

    public  void greetPet ( Pet pet){

        System.out.println("Привет " + pet.getNickName() );
    }
    public   void  describePet(Pet pet){

        System.out.println( "У меня есть " + pet.getSpecies() +", ему "+ pet.getAge() + " лет, он "+ (pet.getTrickLevel() > 50? "очень хитрый":"почти не хитрый"));
    }

    @Override
    public  String toString (){
        String message = "Human{name = " + name + " surname = " + surname +" year = "+ year + " iq = " + iq + " schedule = " + schedule +
                "}" ;
        System.out.println(message);
        return message;
    }
    @Override
    public int hashCode(){
        int result = this.getSurname() == null?0:this.getSurname().hashCode();
        result = result + year;
        result = result + iq;
        return result;
    }
    @Override
    public boolean equals(Object obj){

        if(obj == null){
            return  false;
        }
        if(!(obj.getClass() == Human.class)){
            return false;
        }
        Human human = (Human) obj;
        String humanName = human.getName();
        String humanSurname = human.getSurname();
        Family humanFamily = human.getFamily();
        if((humanName == this.name  || humanName.equals(this.name)) &&
                ( humanSurname == this.surname ||  humanSurname.equals(this.surname))  &&
                (humanFamily == this.family || humanFamily.equals(this.family))) {
            return true;
        }else  return false;

    }

    public static void main(String[] args) {
        Human Sam =new Human("Sam","Smith",32);
        HashMap <DayOfWeek,String> schedule = new HashMap<>();
        Sam.toString();

    }


}
